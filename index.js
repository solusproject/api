const fastify = require('fastify')({ logger: true })
const fs = require('fs')
const natural = require("natural");

/* Build the AI */
const classifier = new natural.BayesClassifier();
const model = require("./model.json");

for (const key of Object.keys(model)) {
    for (const string of model[key]) {
        classifier.addDocument(string, key);
    }
}

/* Training */
classifier.train();

/* Start the server */
const start = async classifier => {
    try {
        await fastify.listen(80)
    } catch (err) {
        fastify.log.error(err)
        process.exit(1)
    }
}

start(classifier)

/* Routes */
fastify.get('/', async (request, reply) => {
    return { ip: request.ip }
})

fastify.post('/rate', async (request, reply) => {
    console.dir(request.body)

    let rating = classifier.classify(request.body.message)

    if (rating == 6) {
        rating = 0
    }
    return { rating: rating }
})

fastify.post('/report', async (request, reply) => {
        fs.appendFileSync('reports.csv', `${request.body.username},${request.body.message}`)
})